package com.example.amtbook.reading_android.feature.read.view;

import com.example.amtbook.reading_android.common.entity.Book;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface IReadView {
    void displayBook(Book book);

    void updateOnLiked();

    void updateOnDisliked();
}
