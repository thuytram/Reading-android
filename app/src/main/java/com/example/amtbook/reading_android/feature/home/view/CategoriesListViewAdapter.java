package com.example.amtbook.reading_android.feature.home.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Category;

import java.util.zip.Inflater;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ThuyTram on 5/6/2016.
 */
public class CategoriesListViewAdapter extends ArrayAdapter<Category> {
    private Context mContext;

    public CategoriesListViewAdapter(Context context) {
        super(context, 0);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Category category = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_categories,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.setUpItem(category);
        return convertView;
    }
    public class ViewHolder{
        @Bind(R.id.text_view_category) TextView mTextViewCategory;
        @Bind(R.id.layout_text_view_categories) LinearLayout mCategoriesListView;
        public ViewHolder(View view){
            ButterKnife.bind(this,view);
        }
        public void setUpItem(Category category){
            mTextViewCategory.setText(category.getCategoryName());

        }

    }
}
