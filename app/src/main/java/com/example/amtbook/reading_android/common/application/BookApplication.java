package com.example.amtbook.reading_android.common.application;

import android.app.Application;
import android.content.Context;

import com.example.amtbook.reading_android.common.entity.Account;
import com.example.amtbook.reading_android.common.view.TypefaceUtil;

/**
 * Created by devalt on 4/28/2016.
 */
public class BookApplication extends Application {
    private static BookApplication mBookApplication;
    private Account mAccount;
    private boolean mIsLogin;
    private Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        this.mContext = getApplicationContext();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/UVF Lobster12.ttf");
        mBookApplication = this;
    }

    public static BookApplication getInstance() {
        if (mBookApplication == null) {
            mBookApplication = new BookApplication();
        }
        return mBookApplication;
    }

    public Context getContext() {
        return mContext;
    }

    public Account getCurrentAccount() {
        return mAccount;
    }

    public void loginAccount(Account currentAccount) {
        if (currentAccount != null) {
            mIsLogin = true;
        }
        this.mAccount = currentAccount;
    }

    public void logoutAccount() {
        this.mAccount = null;
        mIsLogin = false;
    }
}
