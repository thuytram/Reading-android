package com.example.amtbook.reading_android.feature.home.listenner;

import com.example.amtbook.reading_android.common.entity.Book;

import java.util.List;

/**
 * Created by devalt on 4/28/2016.
 */
public interface GetAllBooksListener {
    void onDone(List<Book> books);

    void onFail(String message);
}
