package com.example.amtbook.reading_android.feature.home.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThuyTram on 4/27/2016.
 */
public class DetailListViewFragment extends Fragment {
    private Context mContext;
    ListView mListView;
    ListDetailBookAdapter mListDetailBookAdapter;
    public List<Book> books = new ArrayList<>();
    //1 : HomeFragment
    //2 : TopFragment
    private int fragmentType;

    OnDetailItemClickListener mOnDetailItemClickListener;

    public interface OnDetailItemClickListener {
        void onDetailItemClick(int position, int fragmentType);
    }

    public void setListener(OnDetailItemClickListener listener) {
        mOnDetailItemClickListener = listener;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
        if (mListDetailBookAdapter != null) {
            mListDetailBookAdapter.clear();
            mListDetailBookAdapter.addAll(books);
        }
    }

    public int getFragmentType() {
        return this.fragmentType;
    }

    public void setFragmentType(int type) {
        this.fragmentType = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mContext = this.getActivity().getApplicationContext();
        return inflater.inflate(R.layout.fragment_detail_listview, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            mListView = (ListView) getView().findViewById(R.id.list_view_detail);
        }
        mListDetailBookAdapter = new ListDetailBookAdapter(mContext);
        mListDetailBookAdapter.clear();
        mListDetailBookAdapter.addAll(books);
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (BookApplication.getInstance().getCurrentAccount() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setCancelable(true);
                    builder.setMessage("This feature coming soon...");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
                return false;
            }
        });
        mListView.setAdapter(mListDetailBookAdapter);

        mListView.setDivider(null);
        mListView.setDividerHeight(0);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mOnDetailItemClickListener.onDetailItemClick(position, DetailListViewFragment.this.getFragmentType());
            }
        });

    }
}
