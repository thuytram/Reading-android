package com.example.amtbook.reading_android.feature.categoryDetail.listenner;

import com.example.amtbook.reading_android.common.entity.Book;

import java.util.List;

/**
 * Created by devalt on 5/10/2016.
 */
public interface GetBooksByCatIdListener {
    void onDone(List<Book> books);

    void onFail(String message);
}
