package com.example.amtbook.reading_android.feature.categoryDetail.presenter;

import android.content.Intent;

import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.feature.categoryDetail.listenner.GetBooksByCatIdListener;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ICategoryDetailPresenter {
    void onDetailItemClickListener(Book book);

    void getCat(Intent it);
}
