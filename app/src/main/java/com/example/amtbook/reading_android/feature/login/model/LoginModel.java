package com.example.amtbook.reading_android.feature.login.model;

import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Account;
import com.example.amtbook.reading_android.common.model.DataAccessModel;
import com.example.amtbook.reading_android.feature.login.listenner.AccountListener;
import com.example.amtbook.reading_android.feature.login.listenner.LoginListener;

import java.util.HashMap;

public class LoginModel implements ILoginModel {
    private static ILoginModel mLoginModel;

    private LoginModel() {

    }

    public static ILoginModel getInstance() {
        if (mLoginModel == null) {
            mLoginModel = new LoginModel();
        }
        return mLoginModel;
    }

    @Override
    public void login(final String username, final String password, final LoginListener loginListener) {
        DataAccessModel.getInstance(BookApplication.getInstance().getContext())
                .getAccount(new AccountListener() {
                    @Override
                    public void onDone(HashMap<String, Account> accounts) {
                        if (accounts.containsKey(username)
                                && accounts.get(username).getPassword().equals(password)) {
                            loginListener.onDone(accounts.get(username));
                        } else {
                            loginListener.onFail(username);
                        }
                    }

                    @Override
                    public void onFail(String message) {
                        loginListener.onFail(username);
                    }
                });
    }
}
