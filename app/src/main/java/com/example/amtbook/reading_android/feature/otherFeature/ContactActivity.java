package com.example.amtbook.reading_android.feature.otherFeature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.view.DialogView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ContactActivity extends AppCompatActivity {

    @Bind(R.id.dialog_contact)
    DialogView mDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);
        mDialogView.setTitleDialog("Contact");
        mDialogView.setMessageDialog("admin@book.com"
                + "\n" + "0123456789");
        mDialogView.setTitleButtonOk("");
        mDialogView.setTitleButtonCancel("OK");
        mDialogView.setOnCancelClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
