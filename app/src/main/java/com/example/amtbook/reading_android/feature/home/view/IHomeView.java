package com.example.amtbook.reading_android.feature.home.view;

import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.entity.Category;

import java.util.List;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface IHomeView {
    //    public void setUpTabFragment(List<Book> books, List<Book> topBooks);
    void updateBook(List<Book> books, List<Book> topBook);

    void updateCategory(List<Category> categories);

    void displayError(String error);
}
