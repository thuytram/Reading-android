package com.example.amtbook.reading_android.feature.search.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.feature.home.view.ListDetailBookAdapter;
import com.example.amtbook.reading_android.feature.search.presenter.ISearchPresenter;
import com.example.amtbook.reading_android.feature.search.presenter.SearchPresenter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity implements ISearchView {

    private ISearchPresenter mSearchPresenter;
    private ListDetailBookAdapter mListDetailBookAdapter;
    @Bind(R.id.list_view_book_by_key_word)
    ListView mListViewBooks;
    @Bind(R.id.edit_text_search)
    SearchView mSearchView;

//    @OnClick(R.id.button_back)
//    void onBack() {
//        finish();
//    }

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        mSearchPresenter = new SearchPresenter(this);
        setupListBookByKeyword();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.label_loading));
    }

    private void setupListBookByKeyword() {
        mListDetailBookAdapter = new ListDetailBookAdapter(this);
        mListViewBooks.setAdapter(mListDetailBookAdapter);
        mListViewBooks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSearchPresenter.onDetailItemClickListener(mListDetailBookAdapter.getItem(position));
            }
        });
        setupSearchBar();
    }

    private void setupSearchBar() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mProgressDialog.show();
                mSearchPresenter.getBooksByKeyWord(mSearchView.getQuery().toString());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public void displayBooks(List<Book> books) {
        mListDetailBookAdapter.clear();
        mListDetailBookAdapter.addAll(books);
        mListDetailBookAdapter.notifyDataSetChanged();
        mProgressDialog.dismiss();
    }

    @Override
    public void displayError(String message) {
        mProgressDialog.dismiss();
        Toast.makeText(SearchActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
