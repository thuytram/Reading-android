package com.example.amtbook.reading_android.feature.search.view;

import com.example.amtbook.reading_android.common.entity.Book;

import java.util.List;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ISearchView {
    void displayBooks(List<Book> books);

    void displayError(String message);
}
