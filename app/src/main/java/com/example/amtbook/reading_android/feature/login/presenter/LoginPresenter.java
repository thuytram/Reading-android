package com.example.amtbook.reading_android.feature.login.presenter;

import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Account;
import com.example.amtbook.reading_android.feature.login.listenner.LoginListener;
import com.example.amtbook.reading_android.feature.login.model.ILoginModel;
import com.example.amtbook.reading_android.feature.login.model.LoginModel;
import com.example.amtbook.reading_android.feature.login.view.ILoginView;

/**
 * Created by devanhlt on 21/04/2016.
 */
public class LoginPresenter implements ILoginPresenter {
    ILoginView mILoginView;
    ILoginModel mILoginModel;

    public LoginPresenter(ILoginView iLoginView) {
        this.mILoginView = iLoginView;
        this.mILoginModel = LoginModel.getInstance();
    }


    @Override
    public void login(String username, String password) {
        mILoginModel.login(username, password, new LoginListener() {
            @Override
            public void onDone(Account account) {
                BookApplication.getInstance().loginAccount(account);
                mILoginView.LoginSuccess(account);
            }

            @Override
            public void onFail(String username) {
                mILoginView.LoginFail(username);
            }
        });
    }
}
