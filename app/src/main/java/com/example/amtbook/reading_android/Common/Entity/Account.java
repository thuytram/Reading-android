package com.example.amtbook.reading_android.common.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by devalt on 4/28/2016.
 */
public class Account implements Parcelable {
    private int id;
    private String password;
    private String createDate;
    private String username;
    private String email;
    private String phone;

    public Account() {

    }

    protected Account(Parcel in) {
        id = in.readInt();
        password = in.readString();
        createDate = in.readString();
        username = in.readString();
        email = in.readString();
        phone = in.readString();
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(password);
        dest.writeString(createDate);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeString(phone);
    }
}
