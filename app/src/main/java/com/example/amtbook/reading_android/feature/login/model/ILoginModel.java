package com.example.amtbook.reading_android.feature.login.model;

import com.example.amtbook.reading_android.feature.login.listenner.LoginListener;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ILoginModel {
    void login(String username, String password, LoginListener loginListener);
}
