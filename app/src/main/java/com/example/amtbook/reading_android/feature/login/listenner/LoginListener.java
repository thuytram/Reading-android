package com.example.amtbook.reading_android.feature.login.listenner;

import com.example.amtbook.reading_android.common.entity.Account;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface LoginListener {
    void onDone(Account account);

    void onFail(String username);
}
