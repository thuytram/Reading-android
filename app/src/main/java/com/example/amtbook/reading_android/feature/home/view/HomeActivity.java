package com.example.amtbook.reading_android.feature.home.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.entity.Category;
import com.example.amtbook.reading_android.feature.home.presenter.HomePresenter;
import com.example.amtbook.reading_android.feature.home.presenter.IHomePresenter;
import com.example.amtbook.reading_android.feature.login.view.LoginActivity;
import com.example.amtbook.reading_android.feature.otherFeature.AboutMeActivity;
import com.example.amtbook.reading_android.feature.otherFeature.ContactActivity;
import com.example.amtbook.reading_android.feature.otherFeature.DevInfoActivity;
import com.example.amtbook.reading_android.feature.otherFeature.HelpActivity;
import com.example.amtbook.reading_android.feature.search.view.SearchActivity;

import java.util.List;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IHomeView,
        CategoriesFragment.OnCategoryItemClickListener,
        DetailListViewFragment.OnDetailItemClickListener {
    private IHomePresenter mPresenter;
    private NavigationView mNavigationView;
    private DetailListViewFragment mHomeFragment;
    private DetailListViewFragment mTopFragment;
    private CategoriesFragment mCategoriesFragment;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new HomePresenter(HomeActivity.this);
        setupNavView();
        setUpTabFragment();
        setupToolbar();
        mPresenter.getData();
        setupProgress();
    }

    private void setupProgress() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.label_loading));
        mProgressDialog.show();
    }

    public void setUpTabFragment() {
        TabLayout mSlidingTabLayout = (TabLayout) findViewById(R.id.tab_layout);

        ViewPager mViewPager = (ViewPager) findViewById(R.id.view_pager);

        mHomeFragment = new DetailListViewFragment();
        mHomeFragment.setFragmentType(1);
        mHomeFragment.setListener(this);

        mTopFragment = new DetailListViewFragment();
        mTopFragment.setFragmentType(2);
        mTopFragment.setListener(this);

        mCategoriesFragment = new CategoriesFragment();
        mCategoriesFragment.setListener(this);


        FragmentAdapter fragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        fragmentAdapter.addFragment(mHomeFragment, "Home");
        fragmentAdapter.addFragment(mTopFragment, "Top");
        fragmentAdapter.addFragment(mCategoriesFragment, "Category");
        if (mViewPager != null)
            mViewPager.setAdapter(fragmentAdapter);
        if (mSlidingTabLayout != null)
            mSlidingTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void updateBook(List<Book> books, List<Book> topBook) {
        mProgressDialog.dismiss();
        mHomeFragment.setBooks(books);
        mTopFragment.setBooks(topBook);
    }

    @Override
    public void updateCategory(List<Category> categories) {
        mCategoriesFragment.setCategories(categories);
    }

    @Override
    public void displayError(String error) {
        mProgressDialog.dismiss();
        new AlertDialog.Builder(this)
                .setMessage("Check your internet connection and retry")
                .setTitle("Connection Error")
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        if (drawer != null)
            drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setupNavView() {
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        if (mNavigationView != null)
            mNavigationView.setNavigationItemSelectedListener(this);
        notifyNavigation();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    void notifyNavigation() {
        MenuItem login = mNavigationView.getMenu().getItem(0);
        MenuItem aboutMe = mNavigationView.getMenu().getItem(1);
        if (BookApplication.getInstance().getCurrentAccount() != null) {
            login.setTitle("Logout");
            aboutMe.setVisible(true);
        } else {
            login.setTitle("Login");
            aboutMe.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            Intent searchIntent = new Intent(this, SearchActivity.class);
            startActivity(searchIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_login) {
            if (BookApplication.getInstance().getCurrentAccount() == null) {
                startActivityForResult(new Intent(this, LoginActivity.class), 0);
            } else {
                Toast.makeText(HomeActivity.this, "Logout", Toast.LENGTH_SHORT).show();
                BookApplication.getInstance().logoutAccount();
                notifyNavigation();
            }
        } else if (id == R.id.nav_about_me) {
            startActivity(new Intent(this, AboutMeActivity.class));
        } else if (id == R.id.nav_contact) {
            startActivity(new Intent(this, ContactActivity.class));
        } else if (id == R.id.nav_dev_info) {
            startActivity(new Intent(this, DevInfoActivity.class));
        } else if (id == R.id.nav_help) {
            startActivity(new Intent(this, HelpActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null)
            drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void OnCategoryItemClick(int position) {
        mPresenter.onCategoryItemClickListener(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        notifyNavigation();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDetailItemClick(int position, int fragmentType) {
        mPresenter.onDetailItemClickListener(position, fragmentType);
    }
}
