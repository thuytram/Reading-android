package com.example.amtbook.reading_android.feature.home.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ThuyTram on 4/28/2016.
 */
public class CategoriesFragment extends Fragment {
    Context mContext;
    List<Category> mCategories = new ArrayList<>();
    CategoriesListViewAdapter adapter;
    OnCategoryItemClickListener mOnCategoryItemClickListener;

    public interface OnCategoryItemClickListener {
        void OnCategoryItemClick(int position);
    }

    public void setListener(OnCategoryItemClickListener listener) {
        mOnCategoryItemClickListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = this.getActivity();
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        ListView mCategoriesListView = (ListView) view.findViewById(R.id.list_view_categories);
        adapter = new CategoriesListViewAdapter(mContext);
        adapter.clear();
        adapter.addAll(mCategories);
        mCategoriesListView.setAdapter(adapter);
        mCategoriesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mOnCategoryItemClickListener.OnCategoryItemClick(position);
            }
        });
        mCategoriesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (BookApplication.getInstance().getCurrentAccount() != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setCancelable(true);
                    builder.setMessage("This feature coming soon...");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
                return false;
            }
        });
        return view;
    }

    public void setCategories(List<Category> categories) {
        this.mCategories = categories;
        if (adapter != null) {
            adapter.clear();
            adapter.addAll(mCategories);
            adapter.notifyDataSetChanged();
        }
    }
}

