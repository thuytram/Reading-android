package com.example.amtbook.reading_android.feature.home.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Book;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ThuyTram on 5/5/2016.
 */
public class DetailViewHolder {
    @Bind(R.id.image_view_book) ImageView mImageViewBook;
    @Bind(R.id.text_view_title) TextView mTextViewTitle;
    @Bind(R.id.text_view_author) TextView mTextViewAuthor;
    @Bind(R.id.text_view_view_count) TextView mTextViewCount;
    @Bind(R.id.text_view_extract) TextView mTextViewExtract;

    public DetailViewHolder(View view){
        ButterKnife.bind(this,view);
    }

    public void setUpItem(Book book, Context context){
        if(book != null){
            Picasso.with(context).load(book.getImage()).into(mImageViewBook);
            mTextViewTitle.setText(book.getName());
            mTextViewAuthor.setText(book.getAuthor());
            mTextViewCount.setText(book.getNumsRead() + "");
            mTextViewExtract.setText(book.getContent().substring(0,200));
        }
    }

}
