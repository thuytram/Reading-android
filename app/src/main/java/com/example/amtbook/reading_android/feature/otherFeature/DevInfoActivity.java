package com.example.amtbook.reading_android.feature.otherFeature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.view.DialogView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DevInfoActivity extends AppCompatActivity {

    @Bind(R.id.dialog_dev_info)
    DialogView mDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_info);
        ButterKnife.bind(this);
        mDialogView.setTitleDialog("Dev Info");
        mDialogView.setMessageDialog("Phạm Thế Mỹ"
                + "\n" + "Nguyễn Thị Thùy Trâm"
                + "\n" + "Lê Tuấn Anh");
        mDialogView.setTitleButtonOk("");
        mDialogView.setTitleButtonCancel("OK");
        mDialogView.setOnCancelClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
