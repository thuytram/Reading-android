package com.example.amtbook.reading_android.feature.home.presenter;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface IHomePresenter {
    public void getData();
    public void onCategoryItemClickListener(int position);
    public void onDetailItemClickListener(int position, int fragmentType);
}
