package com.example.amtbook.reading_android.feature.otherFeature;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.amtbook.reading_android.R;

/**
 * Created by devalt on 5/6/2016.
 */
public class SlidePageFragment extends Fragment {

    private ImageView mImageView;
    private Integer mImg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_screen_help, container, false);
        mImageView = (ImageView) rootView.findViewById(R.id.image_view_help);
        mImageView.setImageResource(mImg);
        return rootView;
    }

    public void setImg(Integer img) {
        this.mImg = img;
    }
}