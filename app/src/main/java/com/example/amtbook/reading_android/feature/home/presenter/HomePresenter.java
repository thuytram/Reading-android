package com.example.amtbook.reading_android.feature.home.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.entity.Category;
import com.example.amtbook.reading_android.feature.categoryDetail.view.CategoryDetailActivity;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllBooksListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllCategoryListener;
import com.example.amtbook.reading_android.feature.home.listenner.HomeActionListener;
import com.example.amtbook.reading_android.feature.home.model.HomeModel;
import com.example.amtbook.reading_android.feature.home.model.IHomeModel;
import com.example.amtbook.reading_android.feature.home.view.IHomeView;
import com.example.amtbook.reading_android.feature.read.view.ReadActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by devanhlt on 21/04/2016.
 */
public class HomePresenter implements IHomePresenter, HomeActionListener {
    private IHomeView mView;
    private IHomeModel mModel;
    private List<Book> books, topBooks;
    private List<Category> categories;
    private Context mContext;

    public static String INTENT_DETAIL_BOOK = "detail_book";
    public static String INTENT_DETAIL_CAT = "detail_cat";

    public HomePresenter(IHomeView iHomeView) {
        this.mView = iHomeView;
        this.mModel = HomeModel.getInstance();
        this.mContext = BookApplication.getInstance().getApplicationContext();
    }

    @Override
    public void getData() {
        this.getBooks();
        this.getCategories();
    }

    @Override
    public void onCategoryItemClickListener(int position) {
        Category category = categories.get(position);
        Intent intent = new Intent((Activity) mView, CategoryDetailActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable(HomePresenter.INTENT_DETAIL_CAT, category);

        intent.putExtras(bundle);
        ((Activity) mView).startActivity(intent);
    }

    @Override
    public void onDetailItemClickListener(int position, int fragmentType) {
        Book book;
        if (fragmentType == 1) {
            book = books.get(position);
        } else book = topBooks.get(position);
        Intent intent = new Intent((Activity) mView, ReadActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable(HomePresenter.INTENT_DETAIL_BOOK, book);

        intent.putExtras(bundle);
        ((Activity) mView).startActivity(intent);
    }


    private void getBooks() {
        mModel.getAllBooks(mContext, new GetAllBooksListener() {
            @Override
            public void onDone(List<Book> books) {
                HomePresenter.this.books = books;
                topBooks = HomePresenter.this.getTopBooks();
                mView.updateBook(books, topBooks);

            }

            @Override
            public void onFail(String message) {
                mView.displayError(message);
            }
        });
    }

    public List<Category> getCategories() {
        mModel.getAllCategory(mContext, new GetAllCategoryListener() {
            @Override
            public void onDone(List<Category> categories) {
                HomePresenter.this.categories = categories;
                mView.updateCategory(categories);
            }

            @Override
            public void onFail(String error) {
                mView.displayError(error);
            }
        });
        return categories;
    }

    private List<Book> getTopBooks() {
        List<Book> topBook = new ArrayList<>();
        topBook.addAll(books);
        this.sortList(topBook);
        return topBook;
    }

    private void sortList(List<Book> books) {
        Collections.sort(books, new Comparator<Book>() {
            @Override
            public int compare(Book lhs, Book rhs) {
                if (lhs.getNumsRead() < rhs.getNumsRead()) {
                    return 1;
                } else {
                    if (lhs.getNumsRead() == rhs.getNumsRead()) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
            }
        });
    }
}
