package com.example.amtbook.reading_android.feature.home.model;

import android.content.Context;

import com.example.amtbook.reading_android.common.model.DataAccessModel;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllBooksListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllCategoryListener;

/**
 * Created by devanhlt on 21/04/2016.
 */
public class HomeModel implements IHomeModel {
    private static IHomeModel mIHomeModel;

    private HomeModel() {

    }

    public static IHomeModel getInstance() {
        if (mIHomeModel == null) {
            mIHomeModel = new HomeModel();
        }
        return mIHomeModel;
    }

    @Override
    public void getAllBooks(Context context, GetAllBooksListener getAllBooksListener) {
        DataAccessModel.getInstance(context).getAllBooks(getAllBooksListener);
    }

    @Override
    public void getAllCategory(Context context, GetAllCategoryListener getAllCategoryListener) {
        DataAccessModel.getInstance(context).getAllCategory(getAllCategoryListener);
    }
}
