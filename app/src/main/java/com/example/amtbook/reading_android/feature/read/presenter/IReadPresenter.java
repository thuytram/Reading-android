package com.example.amtbook.reading_android.feature.read.presenter;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface IReadPresenter {
    void updateBookNumLike(int num);
}
