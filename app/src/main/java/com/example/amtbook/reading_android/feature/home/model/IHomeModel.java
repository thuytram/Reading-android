package com.example.amtbook.reading_android.feature.home.model;

import android.content.Context;

import com.example.amtbook.reading_android.feature.home.listenner.GetAllBooksListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllCategoryListener;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface IHomeModel {
    void getAllBooks(Context context, GetAllBooksListener getAllBooksListener);

    void getAllCategory(Context context, GetAllCategoryListener getAllCategoryListener);
}
