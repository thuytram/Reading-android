package com.example.amtbook.reading_android.feature.otherFeature;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.application.BookApplication;
import com.example.amtbook.reading_android.common.entity.Account;
import com.example.amtbook.reading_android.common.view.DialogView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AboutMeActivity extends AppCompatActivity {

    @Bind(R.id.dialog_about_me)
    DialogView mDialogView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        ButterKnife.bind(this);
        mDialogView.setTitleDialog("About me");
        Account account = BookApplication.getInstance().getCurrentAccount();
        mDialogView.setMessageDialog(account.getEmail()
                + "\n" + account.getUsername()
                + "\n" + account.getPassword());
        mDialogView.setTitleButtonOk("");
        mDialogView.setTitleButtonCancel("OK");
        mDialogView.setOnCancelClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
