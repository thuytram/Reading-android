package com.example.amtbook.reading_android.feature.read.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.feature.read.presenter.IReadPresenter;
import com.example.amtbook.reading_android.feature.read.presenter.ReadPresenter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ReadActivity extends AppCompatActivity implements IReadView {

    IReadPresenter mReadPresenter;
    @Bind(R.id.text_view_book)
    TextView mTextViewBook;
    @Bind(R.id.tool_bar_read)
    Toolbar mToolbar;
    private ActionBar mActionBar;
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mReadPresenter = new ReadPresenter(getIntent(), this);
    }

    @Override
    public void displayBook(Book book) {
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle(book.getName());
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
        }
        mTextViewBook.setTypeface(Typeface.SANS_SERIF);
        mTextViewBook.setText("\n" + book.getContent().replace("\n", "\n  "));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
//            case R.id.action_like:
//                if () {
//                    item.setIcon(R.drawable.ic_liked);
//                    mReadPresenter.updateBookNumLike(1);
//                } else {
//                    mReadPresenter.updateBookNumLike(-1);
//                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateOnLiked() {
        mMenu.getItem(0).setIcon(R.drawable.ic_liked);
        Log.d("LOG", "liked");
    }

    @Override
    public void updateOnDisliked() {
        mMenu.getItem(0).setIcon(R.drawable.ic_not_like);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.read_menu, menu);
//        this.mMenu = menu;
//        return super.onCreateOptionsMenu(menu);
//    }
}
