package com.example.amtbook.reading_android.common.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {
    private String id;
    private String name;
    private String author;
    private String content;
    private String image;
    private int numOfChapter;
    private int numsRead;
    private int numsLike;
    private int categoryId;

    protected Book(Parcel in) {
        id = in.readString();
        name = in.readString();
        author = in.readString();
        content = in.readString();
        image = in.readString();
        numOfChapter = in.readInt();
        numsRead = in.readInt();
        numsLike = in.readInt();
        categoryId = in.readInt();
    }

    public Book() {

    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNumOfChapter() {
        return numOfChapter;
    }

    public void setNumOfChapter(int numOfChapter) {
        this.numOfChapter = numOfChapter;
    }

    public int getNumsRead() {
        return numsRead;
    }

    public void setNumsRead(int numsRead) {
        this.numsRead = numsRead;
    }

    public int getNumsLike() {
        return numsLike;
    }

    public void setNumsLike(int numsLike) {
        this.numsLike = numsLike;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(author);
        dest.writeString(content);
        dest.writeString(image);
        dest.writeInt(numOfChapter);
        dest.writeInt(numsRead);
        dest.writeInt(numsLike);
        dest.writeInt(categoryId);
    }
}
