package com.example.amtbook.reading_android.common.model;

import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.listener.UpdateBookListener;
import com.example.amtbook.reading_android.feature.categoryDetail.listenner.GetBooksByCatIdListener;
import com.example.amtbook.reading_android.feature.login.listenner.AccountListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllBooksListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllCategoryListener;
import com.example.amtbook.reading_android.feature.search.listenner.GetBooksByKeyWordListener;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface IDataAccessModel {
    void updateBook(Book book, final UpdateBookListener updateBookListener);

    void getBooksByKeyWord(String keyword, GetBooksByKeyWordListener getBooksByKeyWordListener);

    void getBooksByCatId(String cat_id, GetBooksByCatIdListener getBooksByCatIdListener);

    void getAllBooks(GetAllBooksListener getAllBooksListener);

    void getAllCategory(GetAllCategoryListener getAllCategoryListener);

    void getAccount(AccountListener accountListener);
}
