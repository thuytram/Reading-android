package com.example.amtbook.reading_android.common.model;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.amtbook.reading_android.common.entity.Account;
import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.entity.Category;
import com.example.amtbook.reading_android.common.listener.UpdateBookListener;
import com.example.amtbook.reading_android.feature.categoryDetail.listenner.GetBooksByCatIdListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllBooksListener;
import com.example.amtbook.reading_android.feature.home.listenner.GetAllCategoryListener;
import com.example.amtbook.reading_android.feature.login.listenner.AccountListener;
import com.example.amtbook.reading_android.feature.search.listenner.GetBooksByKeyWordListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataAccessModel implements IDataAccessModel {
    private static Context mContext;
    private static IDataAccessModel mDataAccessModel;
    private RequestQueue mRequestQueue;
    private List<Book> mBooks;
    private List<Category> mCategories;
    private HashMap<String, Account> mAccounts;

    private DataAccessModel() {

    }

    public static IDataAccessModel getInstance(Context context) {
        mContext = context;
        if (mDataAccessModel == null) {
            mDataAccessModel = new DataAccessModel();
        }
        return mDataAccessModel;
    }

    @Override
    public void getBooksByCatId(final String cat_id, final GetBooksByCatIdListener getBooksByCatIdListener) {
        mBooks = new ArrayList<>();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST,
                "http://invader-001-site1.1tempurl.com/truyen.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String r) {
                        JSONArray response = new JSONArray();
                        try {
                            response = new JSONArray(r);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Book book = new Book();
                                book.setId(object.getString("ma_truyen"));
                                book.setName(object.getString("ten_truyen"));
                                book.setAuthor(object.getString("tac_gia"));
                                book.setContent(object.getString("noi_dung"));
                                book.setImage(object.getString("hinh_anh"));
                                book.setNumOfChapter(object.getInt("so_chuong"));
                                book.setNumsRead(object.getInt("luot_doc"));
                                book.setNumsLike(object.getInt("luot_thich"));
                                book.setCategoryId(object.getInt("ma_tl"));
                                mBooks.add(book);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        getBooksByCatIdListener.onDone(mBooks);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getBooksByCatIdListener.onFail(error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("cat_id", cat_id);
                return params;
            }
        };
        getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public void getBooksByKeyWord(final String keyword, final GetBooksByKeyWordListener getBooksByKeyWordListener) {
        mBooks = new ArrayList<>();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST,
                "http://invader-001-site1.1tempurl.com/truyen.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String r) {
                        JSONArray response = new JSONArray();
                        try {
                            response = new JSONArray(r);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Book book = new Book();
                                book.setId(object.getString("ma_truyen"));
                                book.setName(object.getString("ten_truyen"));
                                book.setAuthor(object.getString("tac_gia"));
                                book.setContent(object.getString("noi_dung"));
                                book.setImage(object.getString("hinh_anh"));
                                book.setNumOfChapter(object.getInt("so_chuong"));
                                book.setNumsRead(object.getInt("luot_doc"));
                                book.setNumsLike(object.getInt("luot_thich"));
                                book.setCategoryId(object.getInt("ma_tl"));
                                mBooks.add(book);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        getBooksByKeyWordListener.onDone(mBooks);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getBooksByKeyWordListener.onFail(error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("key_word", keyword.replace(" ", "_"));
                return params;
            }
        };
        getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public void updateBook(final Book book, final UpdateBookListener updateBookListener) {
        mBooks = new ArrayList<>();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST,
                "http://invader-001-site1.1tempurl.com/update_book.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String r) {
                        updateBookListener.onDone(r);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        updateBookListener.onFail(error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", book.getId());
                params.put("tt", book.getName());
                params.put("tg", book.getAuthor());
                params.put("nd", book.getContent());
                params.put("ha", book.getImage());
                params.put("tl", book.getCategoryId() + "");
                params.put("sc", book.getNumOfChapter() + "");
                params.put("ld", book.getNumsRead() + "");
                params.put("lt", book.getNumsLike() + "");
                return params;
            }
        };
        getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public void getAllBooks(final GetAllBooksListener getAllBooksListener) {
        mBooks = new ArrayList<>();
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.POST,
                "http://invader-001-site1.1tempurl.com/truyen.php",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Book book = new Book();
                                book.setId(object.getString("ma_truyen"));
                                book.setName(object.getString("ten_truyen"));
                                book.setAuthor(object.getString("tac_gia"));
                                book.setContent(object.getString("noi_dung"));
                                book.setImage(object.getString("hinh_anh"));
                                book.setNumOfChapter(object.getInt("so_chuong"));
                                book.setNumsRead(object.getInt("luot_doc"));
                                book.setNumsLike(object.getInt("luot_thich"));
                                book.setCategoryId(object.getInt("ma_tl"));
                                mBooks.add(book);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        getAllBooksListener.onDone(mBooks);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getAllBooksListener.onFail(error.getMessage());
            }
        });
        getRequestQueue().add(jsonObjectRequest);
    }

    @Override
    public void getAllCategory(final GetAllCategoryListener getAllCategoryListener) {
        mCategories = new ArrayList<>();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,
                "http://invader-001-site1.1tempurl.com/theloai.php",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Category category = new Category();
                                category.setCategoryId(object.getInt("ma_tl"));
                                category.setCategoryName(object.getString("ten_tl"));
                                mCategories.add(category);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        getAllCategoryListener.onDone(mCategories);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getAllCategoryListener.onFail(error.getMessage());
            }
        });
        getRequestQueue().add(jsonArrayRequest);
    }

    @Override
    public void getAccount(final AccountListener accountListener) {
        mAccounts = new HashMap<>();
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,
                "http://invader-001-site1.1tempurl.com/taikhoan.php",
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject object = response.getJSONObject(i);
                                Account account = new Account();
                                account.setId(object.getInt("id"));
                                account.setUsername(object.getString("ten_tai_khoan"));
                                account.setCreateDate(object.getString("ngay_tao"));
                                account.setEmail(object.getString("email"));
                                account.setPhone(object.getString("sdt"));
                                account.setPassword(object.getString("mat_khau"));
                                mAccounts.put(account.getUsername(), account);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        accountListener.onDone(mAccounts);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                accountListener.onFail(error.getMessage());
            }
        });
        getRequestQueue().add(jsonArrayRequest);
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }
}
