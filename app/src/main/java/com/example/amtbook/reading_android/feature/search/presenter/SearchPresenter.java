package com.example.amtbook.reading_android.feature.search.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.model.DataAccessModel;
import com.example.amtbook.reading_android.common.model.IDataAccessModel;
import com.example.amtbook.reading_android.feature.home.presenter.HomePresenter;
import com.example.amtbook.reading_android.feature.read.view.ReadActivity;
import com.example.amtbook.reading_android.feature.search.listenner.GetBooksByKeyWordListener;
import com.example.amtbook.reading_android.feature.search.view.ISearchView;

import java.util.List;

/**
 * Created by devanhlt on 21/04/2016.
 */
public class SearchPresenter implements ISearchPresenter {
    private ISearchView iSearchView;
    private IDataAccessModel iDataAccessModel;

    public SearchPresenter(ISearchView searchView) {
        this.iSearchView = searchView;
        iDataAccessModel = DataAccessModel.getInstance((Context) iSearchView);
    }

    @Override
    public void getBooksByKeyWord(String keyword) {
        iDataAccessModel.getBooksByKeyWord(keyword, new GetBooksByKeyWordListener() {
            @Override
            public void onDone(List<Book> books) {
                iSearchView.displayBooks(books);
            }

            @Override
            public void onFail(String message) {
                iSearchView.displayError(message);
            }
        });
    }


    @Override
    public void onDetailItemClickListener(Book book) {
        Intent intent = new Intent((Activity) iSearchView, ReadActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable(HomePresenter.INTENT_DETAIL_BOOK, book);

        intent.putExtras(bundle);
        ((Activity) iSearchView).startActivity(intent);
    }
}
