package com.example.amtbook.reading_android.feature.categoryDetail.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.entity.Category;
import com.example.amtbook.reading_android.common.model.DataAccessModel;
import com.example.amtbook.reading_android.common.model.IDataAccessModel;
import com.example.amtbook.reading_android.feature.categoryDetail.listenner.GetBooksByCatIdListener;
import com.example.amtbook.reading_android.feature.categoryDetail.view.ICategoryDetailView;
import com.example.amtbook.reading_android.feature.home.presenter.HomePresenter;
import com.example.amtbook.reading_android.feature.read.view.ReadActivity;

import java.util.List;

/**
 * Created by devanhlt on 21/04/2016.
 */
public class CategoryDetailPresenter implements ICategoryDetailPresenter {
    private ICategoryDetailView mView;
    private IDataAccessModel mData;
    private Category mCategory;

    public CategoryDetailPresenter(ICategoryDetailView mView) {
        this.mView = mView;
        mData = DataAccessModel.getInstance((Context) mView);
    }

    @Override
    public void getCat(Intent it) {
        Bundle bd = it.getExtras();
        mCategory = bd.getParcelable("detail_cat");
        if (mCategory != null) {
            getBooksByCatId(mCategory.getCategoryId() + "");
        }
    }

    public void getBooksByCatId(String cat_id) {
        mData.getBooksByCatId(cat_id, new GetBooksByCatIdListener() {
            @Override
            public void onDone(List<Book> books) {
                mView.displayBooks(books);
                mView.setTitle(mCategory.getCategoryName());
            }

            @Override
            public void onFail(String message) {
                mView.displayError(message);
            }
        });
    }

    @Override
    public void onDetailItemClickListener(Book book) {
        Intent intent = new Intent((Activity) mView, ReadActivity.class);

        Bundle bundle = new Bundle();
        bundle.putParcelable(HomePresenter.INTENT_DETAIL_BOOK, book);

        intent.putExtras(bundle);
        ((Activity) mView).startActivity(intent);
    }
}
