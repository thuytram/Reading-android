package com.example.amtbook.reading_android.common.listener;

/**
 * Created by devalt on 5/12/2016.
 */
public interface UpdateBookListener {
    void onDone(String message);

    void onFail(String message);
}
