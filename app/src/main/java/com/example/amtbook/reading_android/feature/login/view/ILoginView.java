package com.example.amtbook.reading_android.feature.login.view;

import com.example.amtbook.reading_android.common.entity.Account;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ILoginView {
    void LoginSuccess(Account account);

    void LoginFail(String username);
}
