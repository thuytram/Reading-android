package com.example.amtbook.reading_android.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.amtbook.reading_android.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by devalt on 5/4/2016.
 */
public class DialogView extends LinearLayout {
    private Context mContext;
    private LinearLayout mView;
    @Bind(R.id.dialog_title)
    TextView mDialogTitle;
    @Bind(R.id.dialog_message)
    TextView mDialogMessage;
    @Bind(R.id.button_cancel)
    Button mDialogButtonCancel;
    @Bind(R.id.button_ok)
    Button mDialogButtonOk;

    public DialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = (LinearLayout) layoutInflater.inflate(R.layout.dialog_layout, this);
        ButterKnife.bind(mView);
    }

    public void setTitleDialog(String title) {
        mDialogTitle.setText(title);
    }

    public void setMessageDialog(String message) {
        mDialogMessage.setText(message);
    }

    public void setTitleButtonOk(String title) {
        mDialogButtonOk.setText(title);
    }

    public void setTitleButtonCancel(String title) {
        mDialogButtonCancel.setText(title);
    }

    public void setOnOkClickListener(OnClickListener onOkClickListener) {
        mDialogButtonOk.setEnabled(true);
        mDialogButtonOk.setOnClickListener(onOkClickListener);
    }

    public void setOnCancelClickListener(OnClickListener onCancelClickListener) {
        mDialogButtonCancel.setEnabled(true);
        mDialogButtonCancel.setOnClickListener(onCancelClickListener);
    }
}
