package com.example.amtbook.reading_android.feature.login.listenner;

import com.example.amtbook.reading_android.common.entity.Account;

import java.util.HashMap;

/**
 * Created by devalt on 4/28/2016.
 */
public interface AccountListener {
    void onDone(HashMap<String, Account> accounts);

    void onFail(String message);
}
