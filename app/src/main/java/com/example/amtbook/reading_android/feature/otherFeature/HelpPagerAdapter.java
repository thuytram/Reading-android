package com.example.amtbook.reading_android.feature.otherFeature;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by devalt on 5/6/2016.
 */
public class HelpPagerAdapter extends FragmentStatePagerAdapter {
    Context mContext;
    List<Integer> mImgs;

    public HelpPagerAdapter(FragmentManager fm, Context mContext, List<Integer> mImgs) {
        super(fm);
        this.mContext = mContext;
        this.mImgs = mImgs;
    }

    @Override
    public Fragment getItem(int position) {
        SlidePageFragment slidePageFragment = new SlidePageFragment();
        slidePageFragment.setImg(mImgs.get(position));
        return slidePageFragment;
    }

    @Override
    public int getCount() {
        return mImgs.size();
    }
}
