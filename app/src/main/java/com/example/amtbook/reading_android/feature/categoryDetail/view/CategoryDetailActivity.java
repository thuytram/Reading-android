package com.example.amtbook.reading_android.feature.categoryDetail.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.feature.categoryDetail.presenter.CategoryDetailPresenter;
import com.example.amtbook.reading_android.feature.categoryDetail.presenter.ICategoryDetailPresenter;
import com.example.amtbook.reading_android.feature.home.view.ListDetailBookAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CategoryDetailActivity extends AppCompatActivity implements ICategoryDetailView {

    @Bind(R.id.tool_bar_cat_detail)
    Toolbar mToolbar;
    @Bind(R.id.list_view_book_by_cat)
    ListView mListView;
    private ActionBar mActionBar;
    private ListDetailBookAdapter mListDetailBookAdapter;
    private ICategoryDetailPresenter mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        ButterKnife.bind(this);
        mPresenter = new CategoryDetailPresenter(this);
        setSupportActionBar(mToolbar);
        setupToolbar();
        setupListViewCatDetail();

    }

    private void setupListViewCatDetail() {
        mListDetailBookAdapter = new ListDetailBookAdapter(this);
        mListView.setAdapter(mListDetailBookAdapter);
        setupProgress();
        mPresenter.getCat(getIntent());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPresenter.onDetailItemClickListener(mListDetailBookAdapter.getItem(position));
            }
        });
    }

    private void setupProgress() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.label_loading));
        mProgressDialog.show();
    }

    private void setupToolbar() {
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
            mActionBar.setTitle(R.string.label_toolbar_title_cat_detail);
        }
    }

    @Override
    public void setTitle(String title) {
        if (mActionBar != null) {
            mActionBar.setTitle(title);
        }
    }

    @Override
    public void displayBooks(List<Book> books) {
        mProgressDialog.dismiss();
        mListDetailBookAdapter.addAll(books);
        mListDetailBookAdapter.notifyDataSetChanged();
    }

    @Override
    public void displayError(String message) {
        mProgressDialog.dismiss();
        Toast.makeText(CategoryDetailActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
