package com.example.amtbook.reading_android.feature.login.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Account;
import com.example.amtbook.reading_android.feature.login.presenter.ILoginPresenter;
import com.example.amtbook.reading_android.feature.login.presenter.LoginPresenter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements ILoginView {
    ILoginPresenter mILoginPresenter;

    @Bind(R.id.toolbar_login)
    Toolbar mToolbar;
    @Bind(R.id.edit_text_username)
    EditText edtUserName;
    @Bind(R.id.edit_text_password)
    EditText edtPassWord;
    @Bind(R.id.button_login)
    Button btnUserName;
    private ProgressDialog mProgressDialog;

    @OnClick(R.id.button_login)
    void onLogin() {
        String username = edtUserName.getText().toString();
        String password = edtPassWord.getText().toString();
        mILoginPresenter.login(username, password);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Logging in ...");
        mProgressDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setupToolbar();
        mILoginPresenter = new LoginPresenter(this);
    }

    @Override
    public void LoginSuccess(Account account) {
        mProgressDialog.dismiss();
        Toast.makeText(this
                , "Dang nhap thanh cong voi\n"
                        + "Ten dang nhap: " + account.getUsername()
                , Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void LoginFail(String username) {
        mProgressDialog.dismiss();
        Toast.makeText(this
                , "Ten dang nhap: " + username + "\nDang nhap that bai!"
                , Toast.LENGTH_SHORT).show();
    }

    void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }
}
