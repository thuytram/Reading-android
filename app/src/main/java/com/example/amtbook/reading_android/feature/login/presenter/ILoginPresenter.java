package com.example.amtbook.reading_android.feature.login.presenter;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ILoginPresenter {
    void login(String username, String password);
}
