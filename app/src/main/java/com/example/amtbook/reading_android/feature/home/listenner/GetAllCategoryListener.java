package com.example.amtbook.reading_android.feature.home.listenner;

import com.example.amtbook.reading_android.common.entity.Category;

import java.util.List;

/**
 * Created by devalt on 4/28/2016.
 */
public interface GetAllCategoryListener {
    void onDone(List<Category> categories);

    void onFail(String error);
}
