package com.example.amtbook.reading_android.feature.search.listenner;

import com.example.amtbook.reading_android.common.entity.Book;

import java.util.List;

/**
 * Created by devalt on 5/11/2016.
 */
public interface GetBooksByKeyWordListener {
    void onDone(List<Book> books);

    void onFail(String message);
}
