package com.example.amtbook.reading_android.feature.otherFeature;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.amtbook.reading_android.R;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HelpActivity extends AppCompatActivity {

    @Bind(R.id.view_pager_help)
    ViewPager mViewPagerHelp;
    @Bind(R.id.toolbar_help)
    Toolbar mToolbar;
    HelpPagerAdapter mHelpPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Help");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Integer[] a = {
                R.drawable.books_about_me,
                R.drawable.books_top,
                R.drawable.books_search,
                R.drawable.books_read,
                R.drawable.books_navi_not_login,
                R.drawable.books_navi_login,
                R.drawable.books_login,
                R.drawable.books_home,
                R.drawable.books_help,
                R.drawable.books_developer_infomation,
                R.drawable.books_contact,
                R.drawable.books_category
        };
        mHelpPagerAdapter = new HelpPagerAdapter(getSupportFragmentManager(), this, Arrays.asList(a));
        mViewPagerHelp.setAdapter(mHelpPagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
