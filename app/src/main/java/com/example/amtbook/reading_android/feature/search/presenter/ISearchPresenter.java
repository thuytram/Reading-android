package com.example.amtbook.reading_android.feature.search.presenter;

import com.example.amtbook.reading_android.common.entity.Book;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ISearchPresenter {
    void getBooksByKeyWord(String keyword);

    void onDetailItemClickListener(Book book);
}
