package com.example.amtbook.reading_android.feature.home.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.amtbook.reading_android.R;
import com.example.amtbook.reading_android.common.entity.Book;

/**
 * Created by ThuyTram on 4/28/2016.
 */
public class ListDetailBookAdapter extends ArrayAdapter<Book> {
    private Context mContext;
    public ListDetailBookAdapter(Context context) {
        super(context, 0);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Book book = this.getItem(position);
        DetailViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_detail_book, parent, false);
            viewHolder = new DetailViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (DetailViewHolder) convertView.getTag();
        }
        viewHolder.setUpItem(book, mContext);
        return convertView;
    }


}
