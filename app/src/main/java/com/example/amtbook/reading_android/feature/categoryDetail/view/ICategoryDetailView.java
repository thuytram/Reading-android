package com.example.amtbook.reading_android.feature.categoryDetail.view;

import android.content.Intent;

import com.example.amtbook.reading_android.common.entity.Book;

import java.util.List;

/**
 * Created by devanhlt on 21/04/2016.
 */
public interface ICategoryDetailView {
    void setTitle(String title);

    void displayBooks(List<Book> books);

    void displayError(String message);
}
