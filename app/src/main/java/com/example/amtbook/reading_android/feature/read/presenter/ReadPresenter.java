package com.example.amtbook.reading_android.feature.read.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.amtbook.reading_android.common.entity.Book;
import com.example.amtbook.reading_android.common.listener.UpdateBookListener;
import com.example.amtbook.reading_android.common.model.DataAccessModel;
import com.example.amtbook.reading_android.common.model.IDataAccessModel;
import com.example.amtbook.reading_android.feature.read.view.IReadView;

public class ReadPresenter implements IReadPresenter {
    private Intent mIntent;
    private Book mBook;
    private IReadView mReadView;
    private IDataAccessModel mModel;

    public ReadPresenter(Intent mIntent, IReadView iReadView) {
        mModel = DataAccessModel.getInstance((Context) mReadView);
        this.mReadView = iReadView;
        this.mIntent = mIntent;
        this.receiveBookData();
    }

    private void receiveBookData() {
        Bundle bd = mIntent.getExtras();
        mBook = bd.getParcelable("detail_book");
        mReadView.displayBook(mBook);
        updateBookNumRead(mBook);
    }

    private void updateBookNumRead(Book book) {
        book.setNumsRead(book.getNumsRead() + 1);
        mModel.updateBook(book, new UpdateBookListener() {
            @Override
            public void onDone(String message) {
                Log.d("LOG", "done: " + message);
            }

            @Override
            public void onFail(String message) {
                Log.d("LOG", "error: " + message);
            }
        });
    }

    @Override
    public void updateBookNumLike(final int num) {
        mBook.setNumsRead(mBook.getNumsLike() + num);
        mModel.updateBook(mBook, new UpdateBookListener() {
            @Override
            public void onDone(String message) {
                if (num > 0)
                    mReadView.updateOnLiked();
                if (num < 0)
                    mReadView.updateOnDisliked();
            }

            @Override
            public void onFail(String message) {
                Log.d("LOG", "error: " + message);
            }
        });
    }
}
