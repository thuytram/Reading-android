package com.example.amtbook.reading_android.common.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by devalt on 4/28/2016.
 */
public class Category implements Parcelable {
    int categoryId;
    String categoryName;

    public Category() {
    }

    protected Category(Parcel in) {
        categoryId = in.readInt();
        categoryName = in.readString();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryId() {

        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeString(categoryName);
    }
}
